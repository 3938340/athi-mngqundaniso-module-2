
void main(){
  mtnApps apps = new mtnApps();
  apps.showInfo();
  apps.capsAll();
}
// a. creates a class named mtnApps
class mtnApps {
  String appName = "EasyEquities";
  String sector = "Investment";
  String developer = "Charles Savage";
  int year = 2020;
//   use object to display
  void showInfo(){
    print("The name of app: $appName ");
    print("The sector: $sector  ");
    print("The developer: $developer ");
    print("Year won: $year ");
  }
//   b. converts appName to uppercase
  void capsAll(){
    print(appName.toUpperCase());
  }
}
